

function getRandomElement(array){
  return array[Math.floor(Math.random()*array.length)];
}



//var item = items[Math.floor(Math.random()*items.length)];

async function generateLines() {
  let container = document.getElementById("container");

  

  let config = await fetch('config.json')
    .then(res => res.json())
    .catch(err => console.error(err));


  while (container.firstChild) {
      container.removeChild(container.firstChild);
  }


  for (let age of config.ages){
    for (let i=0; i<config.per_block; i++){
      let line = `${getRandomElement(config.names)}, ca. ${age}, ${getRandomElement(config.jobs)}, ${getRandomElement(config.traits)} - ${getRandomElement(config.illness)}`
      let node = document.createElement('p');
      let textNode = document.createTextNode(line);
      node.appendChild(textNode);
      container.appendChild(node);
    }
    container.appendChild(document.createElement('br'));
  }
}


document.addEventListener("DOMContentLoaded", function(event) {
  generateLines();
});

