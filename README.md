#In order to run it:

1. install Python 3
2. fire up a terminal and `cd` to the index.html dir
3. type `python3 -m http.simple`
4. open a browser and navigate to localhost:8000 or 127.0.0.1:8000

#Configuration

Open config.json in a text editor to change the settings.

###A couple of explanations:
`per_block` (default: 3) - Sets the entities per age

`ages` - Add or remove ages. Each age gets a block
